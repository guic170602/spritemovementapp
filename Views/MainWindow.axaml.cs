using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Markup.Xaml;
using Avalonia.Platform;
using NAudio.Wave;
using System;
using System.IO;

namespace SpriteMovementApp.Views
{
    public partial class MainWindow : Window
    {
        private WaveOutEvent _waveOut;
        private AudioFileReader _audioFileReader;
        private IWavePlayer _wavePlayer; 
        public MainWindow()
        {
            InitializeComponent();
            _waveOut = new WaveOutEvent();
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            var img = this.FindControl<Image>("gokuImg");

            if (img == null)
            {
                throw new InvalidOperationException("Imagem n�o encontrada");
            }

            Thickness margin = img.Margin;

            switch (e.Key)
            {
                case Key.Left:
                    margin = new Thickness(margin.Left - 10, margin.Top, 0, 0); // Move 10 pixels para a esquerda
                    break;
                case Key.Right:
                    margin = new Thickness(margin.Left + 10, margin.Top, 0, 0); // Move 10 pixels para a direita
                    break;
                case Key.Up:
                    margin = new Thickness(margin.Left, margin.Top - 10, 0, 0); // Move 10 pixels para cima
                    break;
                case Key.Down:
                    margin = new Thickness(margin.Left, margin.Top + 10, 0, 0); // Move 10 pixels para baixo
                    break;
            }

            // Atualiza a propriedade Margin da imagem
            img.Margin = margin;

            // Toca o som
            PlaySound();
        }

        private void PlaySound()
        {
            using var stream = AssetLoader.Open(new Uri($"avares://SpriteMovementApp/Assets/sound.mp3"));
            if (stream == null)
                throw new InvalidOperationException("Resource not found.");

            // Create a temporary file and copy the asset stream into it.
            var tempFile = Path.GetTempFileName();
            using (var fileStream = File.Create(tempFile))
            {
                stream.CopyTo(fileStream);
            }

            // Initialize and play the audio file using NAudio.
            _wavePlayer = new WaveOutEvent();
            _audioFileReader = new AudioFileReader(tempFile);
            _wavePlayer.Init(_audioFileReader);
            _wavePlayer.Play();

            // Cleanup after playback is complete.
            _wavePlayer.PlaybackStopped += (sender, args) =>
            {
                _audioFileReader.Dispose();
                _wavePlayer.Dispose();
                File.Delete(tempFile);
            };

        }
    }
}
